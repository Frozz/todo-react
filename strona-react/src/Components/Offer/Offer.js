import React from 'react';
import OfferSquare from '../Offer/OfferSquare.js';
import '../Components Styles/Offer.css';

function Offer() {

  return (
    <section className="offer" id="offer">
      <h1 className="container">Czym zajmuje się nasza firma?</h1>
      <div className="container">
        <i className="fas fa-circle"></i>
        <div className="offer-box-wrapper">
          <OfferSquare title="Offer1" />
          <OfferSquare title="Offer2"/>
          <OfferSquare title="Offer3"/>
          <OfferSquare title="Offer4"/>
          <OfferSquare title="Offer5"/>
          <OfferSquare title="Offer6"/>
          <OfferSquare title="Offer7"/>
          <OfferSquare title="Offer8"/>
          <OfferSquare title="Offer9"/>
        </div>
      </div>
    </section >
  )
}

export default Offer;