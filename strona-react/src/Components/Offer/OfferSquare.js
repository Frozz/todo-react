import React from 'react';

function OfferSquare(props) {
  return (
    <div className="offer-box">
      <div className="offer-box-content">
        {props.title} <br></br>
      </div>
    </div>
  )
}

export default OfferSquare;