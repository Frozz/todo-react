import React from 'react';
import AboutElement from './AboutElement.js';
import '../Components Styles/About.css';

function About() {

  return (
    <section id="about" className="about">
      <div className="container">
        <h1 className="h1-dark">Nasze psy</h1>
        <AboutElement title="Hachiko (reproduktor)" img="first" />
        <AboutElement title="Ciri (suka)" img="second"/>
        <AboutElement title="Ksena (suka)" img="third"/>
      </div>
    </section>
  )
}

export default About;