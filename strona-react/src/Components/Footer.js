import React from 'react';
import './Components Styles/Footer.css';

function Footer() {

  return (
    <section className="footer">
      <div className="container">
        <div className="footer-h2">
          <h2>Wilcza Łapa - wszelkie prawa zastrzeżone, 2020</h2>
        </div>
        <div className="footer-icons">
          <a href="#"><i className="fab fa-instagram"></i></a>
          <a href="#"><i className="fab fa-facebook-square"></i></a>
        </div>
      </div>
    </section>
  )
}

export default Footer;