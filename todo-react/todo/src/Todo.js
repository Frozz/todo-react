import React from 'react';
import axios from 'axios';
import './Todo.css';
import Header from './Components/Header/Header';
import AddNewItem from './Components/AddNewItem/AddNewItem';
import Modal from './Components/Modal/Modal';
import TodoItem from './Components/TodoItem/TodoItem';
import Loader from './Components/Loader/Loader';


class App extends React.Component {
  constructor() {
    super();

    this.state = {
      newTodoName: '',
      todoList: [],
      isModalOpen: false,
      editedTodoName: '',
      editedTodoId: '',
      isLoading: true,
    };
  }

  addNewTodo = () => {
    const { newTodoName } = this.state;
    axios.post('http://195.181.210.249:3000/todo/', { title: newTodoName, extra: "0" }).then(() => {
      this.getAndRenderTodos();
      this.setState({ newTodoName: '' })
    });
  }

  updateNewTodoName = event => {
    this.setState({ newTodoName: event.target.value });
  }

  updateEditedTodoName = event => {
    this.setState({ editedTodoName: event.target.value });
  }

  removeTodoAndRenderList = id => {
    axios.delete(`http://195.181.210.249:3000/todo/${id}`).then(() => {
      this.getAndRenderTodos();
    });
  }

  setAsDoneAndRenderList = (id, status) => {
    let extra = status;

    if (extra === "0") {
      extra = "1";
    } else {
      extra = "0";
    }

    axios.put(`http://195.181.210.249:3000/todo/${id}`, { extra }).then(() => {
      this.getAndRenderTodos();
    });
  }

  openEditModal = (title, id) => {
    this.setState({
      editedTodoName: title,
      editedTodoId: id,
      isModalOpen: true,
    });
  }

  closeEditModal = () => {
    this.setState({
      isModalOpen: false,
    })
  }

  updateTodoAndRenderList = () => {
    this.setState({ isLoading: true })

    const { editedTodoId, editedTodoName } = this.state;
    axios.put(`http://195.181.210.249:3000/todo/${editedTodoId}`, { title: editedTodoName }).then(() => {
      this.setState({
        editedTodoName: '',
        editedTodoId: '',
        isModalOpen: false,
      })
      this.getAndRenderTodos();
    })
  }

  getAndRenderTodos = () => {
    axios.get('http://195.181.210.249:3000/todo/').then(resp => {
      const todosFromServer = resp.data;
      this.setState({
        todoList: todosFromServer,
        isLoading: false

      })
    });
  }

  componentDidMount() {
    this.getAndRenderTodos();
  }

  render() {
    return (
      <main className="main">
        <Header />
        <AddNewItem
          updateNewTodoName={this.updateNewTodoName}
          inputValue={this.state.newTodoName}
          addTodo={this.addNewTodo}
        />
        {this.state.isModalOpen && (
          <Modal
            onChange={this.updateEditedTodoName}
            onSubmit={this.updateTodoAndRenderList}
            onClose={this.closeEditModal}
            inputValue={this.state.editedTodoName}
            buttonLabelSave={'Save'}
            buttonLabelClose={'Close'}
          />
        )}

        <section className="main-notes">
          <ul className="main-notes-list">
            {this.state.isLoading && (<div> <Loader /> </div>)}

            {this.state.todoList.map((todo =>
              <TodoItem
                todo={todo}
                key={todo.id}
                onEdit={this.openEditModal}
                onDelete={this.removeTodoAndRenderList}
                onStatusChange={this.setAsDoneAndRenderList}
              />
            ))}
          </ul>
        </section>
      </main>
    );
  }
}

export default App;
