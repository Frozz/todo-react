import React from 'react';
import Button from '../Button/Button';
import './Modal.css';


const Modal = ({ onChange, onSubmit, inputValue, buttonLabelSave, buttonLabelClose, onClose }) => {
    return (
        <div className="modal">
            <input onChange={onChange} value={inputValue} type="text" />
            <div className="buttonsWrapper">
                <Button onClick={onSubmit} label={buttonLabelSave} type={'success'} />
                <Button onClick={onClose} label={buttonLabelClose} />
            </div>
        </div>
    )
}


export default Modal;