import React from 'react';
import './Header.css';

function Header() {

  return (
    <header className="header">
    <div className="header-content">
      <i className="fas fa-check-square"></i>
      <h1>Your TODO List</h1>
      <i className="fas fa-check-square"></i>
    </div>
  </header>
  )
}

export default Header;