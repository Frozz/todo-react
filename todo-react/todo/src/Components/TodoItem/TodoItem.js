import React from 'react';
import './TodoItem.css';
import Button from '../Button/Button.js';

const Todo = ({ todo, onEdit, onDelete, onStatusChange }) => {
    const { title, id, extra } = todo;
   
    return (
        <li>
            <span className={ extra === '1' ? 'todo-done' : ''}>{title} </span>
            <Button onClick={() => { onDelete(id) }} label={'DELETE'} type={'danger'} />
            <Button 
                onClick={() => { onStatusChange(id, extra) }} 
                label={extra === '1' ? 'UNDONE' : 'DONE'}  
                type={extra === '1' ? 'danger' : 'success'} />
            <Button onClick={() => {onEdit(title, id)}} label={'EDIT'}/>
        </li>
    )
}


export default Todo;